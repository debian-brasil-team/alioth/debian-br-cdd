#!/bin/sh
set -e

. /usr/share/debconf/confmodule

# Get the configuration from /etc/apt/apt.conf
APTGET="/usr/bin/apt-get"
APTETC="/etc/apt/"
APTCDROM="/usr/bin/apt-cdrom"

# After mounting a cd, call this function to scan
# it with apt-cdrom, which also adds it to the sources.list.
# The cd will then be unmounted.
scan_cd () {
	clear >/dev/tty </dev/tty
	echo "Verificando CD, por favor aguarde alguns instantes..."
	echo
	# Touch file because apt-cdrom bails if it doesn't exist.
	touch ${APTETC}sources.list
	if $APTCDROM add --no-mount </dev/tty >/dev/tty; then
		umount /cdrom 2>/dev/null || true
		clear >/dev/tty </dev/tty
		return 0
	else
		umount /cdrom 2>/dev/null || true
		clear >/dev/tty </dev/tty
		# Apt didn't like the cdrom for some reason.
		db_fset apt-setup/cd/bad seen false
		db_input critical apt-setup/cd/bad || true
		db_go || true
		return 1
	fi
}

if [ "$1" = "new" ]; then
    # Put our configuration files in their place
    cp -r /usr/share/debian-br-cdd/etc/* /etc/
fi

# Ask about a proxy if no proxy is yet defined.
if [ -z "$http_proxy" ]; then
    if [ ! -e "$APTETC/apt.conf" ] || \
	! grep -iq 'Acquire::http::Proxy' $APTETC/apt.conf; then
	db_fset mirror/http/proxy seen false
	db_input high mirror/http/proxy || true
	db_go || continue
	# Add a trailing slash if missing
	db_get mirror/http/proxy || true
	http_proxy="$(echo $RET | sed 's/[/]*$/\//')"
	db_set mirror/http/proxy "$http_proxy" || true 
	PROXY_OPTS="-o Acquire::http::Proxy=\"$http_proxy\""
    fi
fi

# As a final sanity check, run apt-get update, and catch the
# return code and errors.
tempfile=`tempfile`
clear >/dev/tty </dev/tty
echo "Testando conexao com a internet..."
echo
if ! $APTGET $PROXY_OPTS -o APT::Get::List-Cleanup=false -o Dir::Etc::sourcelist=${APTETC}sources.list update 2>$tempfile; then
    clear >/dev/tty </dev/tty
    sed -i 's,^deb \(http\|ftp\|rsync\),#deb \1,g' $APTETC/sources.list
    db_set mirror/http/proxy ""
    rm -f $tempfile
else
    # Success, so add proxy information if not already present.
    db_get mirror/http/proxy
    if [ "$RET" ]; then
	touch $APTETC/apt.conf
	if ! grep -iq 'Acquire::http::Proxy' $APTETC/apt.conf; then
	    echo "Acquire::http::Proxy \"$RET\";" >> $APTETC/apt.conf
	fi
    fi
fi 

# If /dev/cdrom exists, use it.
if [ -e /dev/cdrom ]; then
	CDDEV=/dev/cdrom
	db_set apt-setup/cd/dev "$CDDEV"
else
	# If there is a fstab entry for /cdrom or /media/cdrom0, use its device.
	CDDEV=$(grep "[[:space:]]/media/cdrom0/*[[:space:]]" /etc/fstab | awk '{print $1}')
	if [ -z "$CDDEV" ]; then
		CDDEV=$(grep "[[:space:]]/cdrom/*[[:space:]]" /etc/fstab | awk '{print $1}')
	fi
	if [ -n "$CDDEV" ] && [ -e "$CDDEV" ]; then
		db_set apt-setup/cd/dev "$CDDEV"
	fi
fi

while true; do
	umount /cdrom 2>/dev/null || true
	# Try mounting the detected cd rom.
	if mount $CDDEV /cdrom -o ro -t iso9660 2>/dev/null && scan_cd; then
	    break
	fi
	db_fset apt-setup/cd/dev seen false
	db_input critical apt-setup/cd/dev || true
	db_go || true
done

# Upgrade the system
apt-get dist-upgrade -y -u
